/////////////////////////////////////////////////////////////////
// calc6.cxx, (c) ATLAS Detector software
/////////////////////////////////////////////////////////////////
// Author: Thomas Gillam (thomas.gillam@cern.ch)
// ExpressionParsing library
/////////////////////////////////////////////////////////////////

#include "ExpressionParser.h"
#include "TestProxyLoader.h"
#include "UnitInterpreterSingleton.h"
#include "NTUPUnitInterpreter.h"

int main()
{
  ExpressionParsing::ProxyLoaderSingleton::setInstance(new ExpressionParsing::TestProxyLoader()); 
  ExpressionParsing::UnitInterpreterSingleton::setInstance(new ExpressionParsing::NTUPUnitInterpreter()); 

  std::cout << "/////////////////////////////////////////////////////////\n\n";
  std::cout << "Expression parser...\n\n";
  std::cout << "/////////////////////////////////////////////////////////\n\n";
  std::cout << "Type an expression...or [q or Q] to quit\n\n";

  typedef std::string::const_iterator iterator_type;
  typedef ExpressionParsing::Grammar<iterator_type> Grammar;
  typedef ExpressionParsing::ast::expression ast_expression;
  typedef ExpressionParsing::Compiler Compiler;

  std::string str;
  while (std::getline(std::cin, str))
  {
    if (str.empty() || str[0] == 'q' || str[0] == 'Q')
      break;

    ExpressionParsing::VirtualMachine mach;      // Our virtual machine
    std::vector<ExpressionParsing::StackElement> code;      // Our VM code
    Grammar calc;            // Our grammar
    ast_expression expression;  // Our program (AST)
    Compiler compile(code);     // Compiles the program

    std::string::const_iterator iter = str.begin();
    std::string::const_iterator end = str.end();
    boost::spirit::ascii::space_type space;
    bool r = phrase_parse(iter, end, calc, space, expression);

    if (r && iter == end)
    {
      std::cout << "-------------------------\n";
      std::cout << "Parsing succeeded\n";
      compile(expression);
      mach.execute(code);
      std::cout << "\nResult: " << mach.top() << std::endl;
      std::cout << "-------------------------\n";
    }
    else
    {
      std::string rest(iter, end);
      std::cout << "-------------------------\n";
      std::cout << "Parsing failed\n";
      std::cout << "-------------------------\n";
    }
  }

  std::cout << "Bye... :-) \n\n";
  return 0;
}
