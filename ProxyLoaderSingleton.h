/////////////////////////////////////////////////////////////////
// ProxyLoaderSingleton.h, (c) ATLAS Detector software
/////////////////////////////////////////////////////////////////
// Author: Thomas Gillam (thomas.gillam@cern.ch)
// ExpressionParsing library
/////////////////////////////////////////////////////////////////

#ifndef PROXY_LOADER_SINGLETON_H
#define PROXY_LOADER_SINGLETON_H

#include "IProxyLoader.h"
#include "TestProxyLoader.h"
#include "EncapsulatingSingleton.h"
#include <stdexcept>

namespace ExpressionParsing {
  template <> struct EncapsulatedDefault<IProxyLoader> {
    typedef TestProxyLoader type;
  };

  class ProxyLoaderSingleton : public EncapsulatingSingleton<IProxyLoader> {
  };
}

#endif // PROXY_LOADER_SINGLETON_H
