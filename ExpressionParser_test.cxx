/////////////////////////////////////////////////////////////////
// ExpressionParser_test.cxx, (c) ATLAS Detector software
/////////////////////////////////////////////////////////////////
// Author: Thomas Gillam (thomas.gillam@cern.ch)
// ExpressionParsing library
/////////////////////////////////////////////////////////////////

#define BOOST_TEST_MODULE templateTest

// Suppress warning in boost test header files
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

#include <boost/test/included/unit_test.hpp>

#ifdef __clang__
#pragma clang diagnostic pop
#endif

#include <vector>
#include <string>
#include <iostream>

#include "ExpressionParser.h"

#define CHECK_SE_VEC_CLOSE(se, vec, tol) \
  do { \
    for (size_t i = 0; i < vec.size(); ++i) { \
      BOOST_CHECK_CLOSE(se.vectorValue<double>()[i], vec[i], tol); \
    } \
  } while (0)

#define CHECK_SE_SE_CLOSE(se1, se2, tol) \
  do { \
    for (size_t i = 0; i < se1.vectorValue<double>().size(); ++i) { \
      BOOST_CHECK_CLOSE(se1.vectorValue<double>()[i], se2.vectorValue<double>()[i], tol); \
    } \
  } while (0)

#include "ParsingInternals.h"

typedef ExpressionParsing::Grammar<std::string::const_iterator> Grammar;
typedef ExpressionParsing::ast::expression ASTExpression;

#include "ProxyLoaderSingleton.h"
#include "TestProxyLoader.h"
#include "UnitInterpreterSingleton.h"
#include "NTUPUnitInterpreter.h"

struct MyConfig {
    MyConfig() { 
      //ExpressionParsing::ProxyLoaderSingleton::setInstance(new ExpressionParsing::TestProxyLoader()); 
      //ExpressionParsing::UnitInterpreterSingleton::setInstance(new ExpressionParsing::NTUPUnitInterpreter()); 
    }
};


BOOST_AUTO_TEST_SUITE(ExpressionParser_test)

BOOST_GLOBAL_FIXTURE(MyConfig)

BOOST_AUTO_TEST_CASE(testBasicParse)
{
  std::string command = "1 + 1";

  Grammar grammar;
  ASTExpression ast;
  using boost::spirit::ascii::space_type;
  std::string::const_iterator begin = command.begin();
  std::string::const_iterator end = command.end();
  bool r = phrase_parse(begin, end, grammar, space_type(), ast);
  BOOST_CHECK(r);
}

BOOST_AUTO_TEST_CASE(testRunVM)
{
  std::string command = "1 + 1";

  Grammar grammar;
  ASTExpression ast;
  using boost::spirit::ascii::space_type;
  std::string::const_iterator begin = command.begin();
  std::string::const_iterator end = command.end();
  bool r = phrase_parse(begin, end, grammar, space_type(), ast);
  BOOST_CHECK(r);

  ExpressionParsing::VirtualMachine vm;
  std::vector<ExpressionParsing::StackElement> code;
  ExpressionParsing::Compiler compiler(code);
  compiler(ast);
  vm.execute(code);
  BOOST_CHECK_EQUAL(vm.top(), 2);
}

BOOST_AUTO_TEST_CASE(testRunVMTwice)
{
  std::string command = "1 + 1";

  Grammar grammar;
  ASTExpression ast;
  using boost::spirit::ascii::space_type;
  std::string::const_iterator begin = command.begin();
  std::string::const_iterator end = command.end();
  bool r = phrase_parse(begin, end, grammar, space_type(), ast);
  BOOST_CHECK(r);

  ExpressionParsing::VirtualMachine vm;
  std::vector<ExpressionParsing::StackElement> code;
  ExpressionParsing::Compiler compiler(code);
  compiler(ast);
  vm.execute(code);
  BOOST_CHECK_EQUAL(vm.top(), 2);

  vm.execute(code);
  BOOST_CHECK_EQUAL(vm.top(), 2);
}

BOOST_AUTO_TEST_CASE(testRunVMSeveralTimesWithProxies)
{
  std::string command = "1 + intTEST";

  Grammar grammar;
  ASTExpression ast;
  using boost::spirit::ascii::space_type;
  std::string::const_iterator begin = command.begin();
  std::string::const_iterator end = command.end();
  bool r = phrase_parse(begin, end, grammar, space_type(), ast);
  BOOST_CHECK(r);

  ExpressionParsing::VirtualMachine vm;
  std::vector<ExpressionParsing::StackElement> code;
  ExpressionParsing::Compiler compiler(code);
  compiler(ast);
  // Depends on implementation in TestProxyLoader
  vm.execute(code);
  BOOST_CHECK_EQUAL(vm.top(), 43);

  vm.execute(code);
  BOOST_CHECK_EQUAL(vm.top(), 44);

  vm.execute(code);
  BOOST_CHECK_EQUAL(vm.top(), 45);
}

BOOST_AUTO_TEST_CASE(testInterface)
{
  ExpressionParsing::ExpressionParser parser;
  parser.loadExpression("1+1");
  BOOST_CHECK_EQUAL(parser.evaluate(), 2);
}

BOOST_AUTO_TEST_CASE(testInterfaceMultipleRun)
{
  ExpressionParsing::ExpressionParser parser;
  parser.loadExpression("1+intTEST");
  BOOST_CHECK_EQUAL(parser.evaluate(), 43);
  BOOST_CHECK_EQUAL(parser.evaluate(), 44);
  BOOST_CHECK_EQUAL(parser.evaluate(), 45);
  BOOST_CHECK_EQUAL(parser.evaluate(), 46);
}

BOOST_AUTO_TEST_CASE(testVectorResult)
{
  ExpressionParsing::ExpressionParser parser;
  parser.loadExpression("1+vectorIntTEST");
  BOOST_CHECK_EQUAL(parser.evaluate(), std::vector<int>(2, 43));
}

BOOST_AUTO_TEST_CASE(testPrecedence)
{
  ExpressionParsing::ExpressionParser parser;
  parser.loadExpression("2*3+1");
  BOOST_CHECK_EQUAL(parser.evaluate(), 7);

  parser.loadExpression("2*3 > 2+7");
  BOOST_CHECK_EQUAL(parser.evaluate(), 0);

  parser.loadExpression("1>3 || 2<7");
  BOOST_CHECK_EQUAL(parser.evaluate(), 1);

  parser.loadExpression("1>3 && 2<7");
  BOOST_CHECK_EQUAL(parser.evaluate(), 0);

  parser.loadExpression("1-3 && 2+7");
  BOOST_CHECK_EQUAL(parser.evaluate(), 1);

  parser.loadExpression("1-1 && 2+7");
  BOOST_CHECK_EQUAL(parser.evaluate(), 0);

  parser.loadExpression("2 * 2 ** 3");
  BOOST_CHECK_EQUAL(parser.evaluate(), 16);

  parser.loadExpression("2 + 2 ** 3");
  BOOST_CHECK_EQUAL(parser.evaluate(), 10);

  parser.loadExpression("2 - 2 ** 3");
  BOOST_CHECK_EQUAL(parser.evaluate(), -6);
}

BOOST_AUTO_TEST_CASE(testEvaluateAsBool)
{
  ExpressionParsing::ExpressionParser parser;
  parser.loadExpression("(2*3+1)>0");
  BOOST_CHECK_EQUAL(parser.evaluateAsBool(), true);

  parser.loadExpression("(2*3+1)==0");
  BOOST_CHECK_EQUAL(parser.evaluateAsBool(), false);

  parser.loadExpression("sin(pi/vectorDoubleTEST)");
  std::vector<double> result(2, 0.07399155168);
  CHECK_SE_VEC_CLOSE(parser.evaluate(), result, 0.0001);
  BOOST_CHECK_THROW(parser.evaluateAsBool(), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(testBoolNames)
{
  ExpressionParsing::ExpressionParser parser;
  parser.loadExpression("(2*3>2) != false");
  BOOST_CHECK_EQUAL(parser.evaluate(), 1);

  parser.loadExpression("(2*3>2) == true");
  BOOST_CHECK_EQUAL(parser.evaluate(), 1);
}

BOOST_AUTO_TEST_CASE(testPowers)
{
  ExpressionParsing::ExpressionParser parser;
  parser.loadExpression("2 ** 2");
  BOOST_CHECK_CLOSE(parser.evaluate(), 4, 0.0001);

  parser.loadExpression("4 ** 0.5");
  BOOST_CHECK_CLOSE(parser.evaluate(), 2, 0.0001);

  parser.loadExpression("4 ** testVector");
  BOOST_CHECK_THROW(parser.evaluate(), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(testExplicitProxyLoader)
{
  ExpressionParsing::ExpressionParser parser(new ExpressionParsing::TestProxyLoader());
  parser.loadExpression("1+intTEST");
  BOOST_CHECK_EQUAL(parser.evaluate(), 43);
  BOOST_CHECK_EQUAL(parser.evaluate(), 44);
  BOOST_CHECK_EQUAL(parser.evaluate(), 45);
  BOOST_CHECK_EQUAL(parser.evaluate(), 46);
}

BOOST_AUTO_TEST_CASE(testUnderscores)
{
  ExpressionParsing::ExpressionParser parser;
  parser.loadExpression("1+int_TEST");
  BOOST_CHECK_EQUAL(parser.evaluate(), 25);
}

BOOST_AUTO_TEST_CASE(testNTUPUnitInterpreter)
{
  ExpressionParsing::ExpressionParser parser(new ExpressionParsing::NTUPUnitInterpreter());
  parser.loadExpression("1 * keV");
  BOOST_CHECK_CLOSE(parser.evaluate(), 0.001, 0.0001);
  parser.loadExpression("1 * MeV");
  BOOST_CHECK_CLOSE(parser.evaluate(), 1, 0.0001);
  parser.loadExpression("1 * GeV");
  BOOST_CHECK_CLOSE(parser.evaluate(), 1000, 0.0001);
  parser.loadExpression("1 * TeV");
  BOOST_CHECK_CLOSE(parser.evaluate(), 1000000, 0.0001);

  parser.loadExpression("1 * mm");
  BOOST_CHECK_CLOSE(parser.evaluate(), 1, 0.0001);
  parser.loadExpression("1 * cm");
  BOOST_CHECK_CLOSE(parser.evaluate(), 10, 0.0001);
}

BOOST_AUTO_TEST_CASE(testNTUPUnitInterpreterFail)
{
  ExpressionParsing::ExpressionParser parser;
  parser.loadExpression("1 * teV");
  BOOST_CHECK_THROW(parser.evaluate(), std::runtime_error);
  parser.loadExpression("1 * tEV");
  BOOST_CHECK_THROW(parser.evaluate(), std::runtime_error);
  parser.loadExpression("1 * mm");
  BOOST_CHECK_CLOSE(parser.evaluate(), 1, 0.0001);
}

BOOST_AUTO_TEST_CASE(testExpressionParserConstructor)
{
  ExpressionParsing::ExpressionParser parser(new ExpressionParsing::TestProxyLoader());
  parser.loadExpression("1 * TeV)");
  BOOST_CHECK_CLOSE(parser.evaluate(), 1000000, 0.0001);

  ExpressionParsing::ExpressionParser parser2(new ExpressionParsing::TestProxyLoader(), new ExpressionParsing::NTUPUnitInterpreter());
  parser2.loadExpression("1 * TeV)");
  BOOST_CHECK_CLOSE(parser2.evaluate(), 1000000, 0.0001);
}

BOOST_AUTO_TEST_CASE(testFunctionCornerCases)
{
  ExpressionParsing::ExpressionParser parser(new ExpressionParsing::TestProxyLoader());
  parser.loadExpression("1/cosh(1.0))");
  BOOST_CHECK_CLOSE(parser.evaluate(), 0.64805427366, 0.0001);
}

BOOST_AUTO_TEST_SUITE_END()
