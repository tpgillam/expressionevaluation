#!/usr/bin/env python
def main():
  targetPrefix = '~/TF2Stuff/17.8.0/'

  makeTempStructure()
  copyFilesToStructure()
  modifyFilesInStructure()
  syncToHEP(fullPath(targetPrefix))
  deleteTempStructure()

def fullPath(prefix):
  import os.path
  return os.path.join(prefix, 'PhysicsAnalysis/DerivationFramework/')


def makeTempStructure():
  import subprocess
  subprocess.call('mkdir DerivationFrameworkTools; mkdir DerivationFrameworkTools/DerivationFrameworkTools; mkdir DerivationFrameworkTools/src', shell=True)

def copyFilesToStructure():
  import subprocess
  for name in _globalHeaders():
    subprocess.call('cp '+name+' DerivationFrameworkTools/DerivationFrameworkTools/', shell=True)
  for name in _sourceFiles():
    subprocess.call('cp '+name+' DerivationFrameworkTools/src/', shell=True)

def _globalHeaders(): 
  return ['ExpressionParser.h', 'IUnitInterpreter.h', 'NTUPUnitInterpreter.h', 'StackElement.h', 'IProxyLoader.h', 'SGNTUPProxyLoader.h', 'StackElement.icc']

def _sourceFiles():
  import os
  for filename in os.listdir('.'):
    extensions = ['.cxx', '.h', '.icc']
    if filename.startswith('.'): continue
    elif filename in _globalHeaders(): continue
    elif '_test' in filename: continue
    elif 'calc6' in filename: continue
    for extension in extensions:
      if filename.endswith(extension): yield filename


def modifyFilesInStructure():
  def lineCallback(fout, line):
    strippedLine = line.strip()
    if not strippedLine.startswith('#include'): return line
    for header in _globalHeaders():
      import os.path
      line = line.replace(header, os.path.join('DerivationFrameworkTools', header))
    return line
  import os.path
  for name in _globalHeaders():
    path = os.path.join('DerivationFrameworkTools/DerivationFrameworkTools', name)
    _modifyFile(path, lineCallback)
  for name in _sourceFiles():
    path = os.path.join('DerivationFrameworkTools/src', name)
    _modifyFile(path, lineCallback)

def _modifyFile(path, lineCallback):
  import tempfile
  fout = tempfile.NamedTemporaryFile(suffix='.tex')
  f = open(path)
  for line in f:
    line = lineCallback(fout, line)
    fout.write(line)
  f.close()
  fout.flush()
  import shutil
  shutil.copyfile(fout.name, path)
  fout.close()


def syncToHEP(targetPath):
  import subprocess
  subprocess.call('scp -r DerivationFrameworkTools pckg:'+targetPath, shell=True)

def deleteTempStructure():
  import subprocess
  subprocess.call('rm -rf DerivationFrameworkTools', shell=True)

if __name__ == '__main__':
  main()
