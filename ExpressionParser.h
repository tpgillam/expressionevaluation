/////////////////////////////////////////////////////////////////
// ExpressionParser.h, (c) ATLAS Detector software
/////////////////////////////////////////////////////////////////
// Author: Thomas Gillam (thomas.gillam@cern.ch)
// ExpressionParsing library
/////////////////////////////////////////////////////////////////

#ifndef EXPRESSION_PARSER_H
#define EXPRESSION_PARSER_H

#include "StackElement.h"
#include "IProxyLoader.h"
#include "IUnitInterpreter.h"

namespace ExpressionParsing {
  class VirtualMachine;

  class ExpressionParser
  {
    public:
      ExpressionParser();
      ExpressionParser(IProxyLoader *proxyLoader);
      ExpressionParser(IUnitInterpreter *unitInterpreter);
      ExpressionParser(IProxyLoader *proxyLoader, IUnitInterpreter *unitInterpreter);
      ~ExpressionParser();

      bool loadExpression(const std::string &expression);
      StackElement evaluate(); 
      bool evaluateAsBool(); 

    private:
      void setup();

    private:
      std::vector<StackElement> code;
      VirtualMachine *vm;
  };
}

#endif // EXPRESSION_PARSER_H
