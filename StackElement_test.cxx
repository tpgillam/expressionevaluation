/////////////////////////////////////////////////////////////////
// StackElement_test.cxx, (c) ATLAS Detector software
/////////////////////////////////////////////////////////////////
// Author: Thomas Gillam (thomas.gillam@cern.ch)
// ExpressionParsing library
/////////////////////////////////////////////////////////////////

#define BOOST_TEST_MODULE templateTest

// Suppress warning in boost test header files
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

#include <boost/test/included/unit_test.hpp>

#ifdef __clang__
#pragma clang diagnostic pop
#endif

#include <vector>
#include <string>
#include <iostream>

#include "StackElement.h"
using ExpressionParsing::StackElement;

#define CHECK_SE_VEC_CLOSE(se, vec, tol) \
  do { \
    for (size_t i = 0; i < vec.size(); ++i) { \
      BOOST_CHECK_CLOSE(se.vectorValue<double>()[i], vec[i], tol); \
    } \
  } while (0)

#define CHECK_SE_SE_CLOSE(se1, se2, tol) \
  do { \
    for (size_t i = 0; i < se1.vectorValue<double>().size(); ++i) { \
      BOOST_CHECK_CLOSE(se1.vectorValue<double>()[i], se2.vectorValue<double>()[i], tol); \
    } \
  } while (0)

#include "ProxyLoaderSingleton.h"
#include "TestProxyLoader.h"
struct MyConfig {
    MyConfig() {
      //ExpressionParsing::ProxyLoaderSingleton::setInstance(new ExpressionParsing::TestProxyLoader());
    }
};


BOOST_AUTO_TEST_SUITE(StackElement_test)

BOOST_GLOBAL_FIXTURE(MyConfig)

BOOST_AUTO_TEST_CASE(testUnknown)
{
  StackElement aUnk;
  BOOST_CHECK_EQUAL(aUnk.getType(), StackElement::SE_UNK);
}

BOOST_AUTO_TEST_CASE(testInt)
{
  StackElement aInt(3);
  BOOST_CHECK_EQUAL(aInt.getType(), StackElement::SE_INT);
  BOOST_CHECK_EQUAL(aInt, 3);
}

BOOST_AUTO_TEST_CASE(testDouble)
{
  StackElement aDouble(3.43);
  BOOST_CHECK_EQUAL(aDouble.getType(), StackElement::SE_DOUBLE);
  BOOST_CHECK_CLOSE(aDouble, 3.43, 0.0001);
}

BOOST_AUTO_TEST_CASE(testIntAddition)
{
  StackElement aInt(3);
  aInt += 4;
  BOOST_CHECK_EQUAL(aInt.getType(), StackElement::SE_INT);
  BOOST_CHECK_EQUAL(aInt, 7);
}

BOOST_AUTO_TEST_CASE(testDoubleAddition)
{
  StackElement aDouble(3.0);
  aDouble += 4.3;
  BOOST_CHECK_EQUAL(aDouble.getType(), StackElement::SE_DOUBLE);
  BOOST_CHECK_CLOSE(aDouble, 7.3, 0.0001);
}

BOOST_AUTO_TEST_CASE(testIntAssignment)
{
  StackElement aInt;
  aInt = 3;
  BOOST_CHECK_EQUAL(aInt.getType(), StackElement::SE_INT);
  BOOST_CHECK_EQUAL(aInt, 3);
}

BOOST_AUTO_TEST_CASE(testIntPromotion)
{
  StackElement a(3);
  a = a + 4.3;
  BOOST_CHECK_EQUAL(a.getType(), StackElement::SE_DOUBLE);
  BOOST_CHECK_CLOSE(a, 7.3, 0.0001);
}

BOOST_AUTO_TEST_CASE(testIntSwitch)
{
  StackElement aInt(3);
  bool pass = false; 
  switch (aInt.asInt()) {
    case 3:
      pass = true;
      break;
    default:
      break;
  }
  BOOST_CHECK(pass);
}

BOOST_AUTO_TEST_CASE(testBadSwitch)
{
  StackElement aUnk;
  StackElement aDouble(3.43);

  BOOST_CHECK_THROW(aUnk.asInt(), std::runtime_error);
  BOOST_CHECK_THROW(aDouble.asInt(), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(testIequalities)
{
  StackElement a(3.4);
  StackElement b(5);
  BOOST_CHECK(a < 5);
  BOOST_CHECK(a < b);
  BOOST_CHECK(a <= 5);
  BOOST_CHECK(a <= b);
  BOOST_CHECK(a != b);
  BOOST_CHECK_EQUAL(a > 5, false);
  BOOST_CHECK_EQUAL(a > b, false);
  BOOST_CHECK_EQUAL(a >= 5, false);
  BOOST_CHECK_EQUAL(a >= b, false);
  BOOST_CHECK_EQUAL(a == b, false);
}

BOOST_AUTO_TEST_CASE(testLogic)
{
  StackElement a(0);
  StackElement b(1);
  BOOST_CHECK_EQUAL(a && a, false);
  BOOST_CHECK_EQUAL(a && b, false);
  BOOST_CHECK_EQUAL(b && a, false);
  BOOST_CHECK_EQUAL(b && b, true);
  BOOST_CHECK_EQUAL(a || a, false);
  BOOST_CHECK_EQUAL(a || b, true);
  BOOST_CHECK_EQUAL(b || a, true);
  BOOST_CHECK_EQUAL(b || b, true);
  BOOST_CHECK_EQUAL(!a, true);
  BOOST_CHECK_EQUAL(!b, false);
}

BOOST_AUTO_TEST_CASE(testEqualities)
{
  StackElement a(3);
  StackElement b(3);
  StackElement c(3.0);
  BOOST_CHECK_EQUAL(a, b);
  BOOST_CHECK_CLOSE(a, c, 0.0001);
}

BOOST_AUTO_TEST_CASE(testTemporaries)
{
  StackElement a(3);
  StackElement b(7);
  StackElement c;
  c = a + b;
  BOOST_CHECK_EQUAL(c, 10);
  BOOST_CHECK_EQUAL(a, 3);
  BOOST_CHECK_EQUAL(b, 7);

  BOOST_CHECK_EQUAL(a+b, 10);
  BOOST_CHECK((a+b) > 7);
}

BOOST_AUTO_TEST_CASE(testMixedTemporaries)
{
  StackElement a(3.4);
  StackElement b(5);
  StackElement c;
  c = a + b;
  BOOST_CHECK_CLOSE(c, 8.4, 0.0001);
  BOOST_CHECK_CLOSE(a+b, 8.4, 0.0001);
  BOOST_CHECK((a+b) > 7);
  BOOST_CHECK_EQUAL((a+b).getType(), StackElement::SE_DOUBLE);
  BOOST_CHECK((a+b) > 8);
  BOOST_CHECK((a+b) >= 8);
}

BOOST_AUTO_TEST_CASE(testDoubleTemporaries)
{
  StackElement a(3.3);
  StackElement b(7.4);
  StackElement c;
  c = a + b;
  BOOST_CHECK_CLOSE(c, 10.7, 0.0001);
  BOOST_CHECK_CLOSE(a, 3.3, 0.0001);
  BOOST_CHECK_CLOSE(b, 7.4, 0.0001);

  BOOST_CHECK_CLOSE(a+b, 10.7, 0.0001);
  BOOST_CHECK((a+b) > 7);
}

BOOST_AUTO_TEST_CASE(testProxy)
{
  StackElement notProxy(3);
  BOOST_CHECK_EQUAL(notProxy.isProxy(), false);

  StackElement intProxy("intTEST");
  BOOST_CHECK_EQUAL(intProxy.getType(), StackElement::SE_UNK);
  BOOST_CHECK_EQUAL(intProxy.isProxy(), true);
  BOOST_CHECK_THROW(intProxy += 3, std::runtime_error);
  intProxy.setValueFromProxy();
  BOOST_CHECK_EQUAL(intProxy.getType(), StackElement::SE_INT);
  BOOST_CHECK_EQUAL(intProxy, 42);
  intProxy.clearValueFromProxy();
  BOOST_CHECK_EQUAL(intProxy.getType(), StackElement::SE_UNK);
  BOOST_CHECK_EQUAL(intProxy.isProxy(), true);

  StackElement doubleProxy("doubleTEST");
  BOOST_CHECK_EQUAL(doubleProxy.getType(), StackElement::SE_UNK);
  BOOST_CHECK_EQUAL(doubleProxy.isProxy(), true);
  doubleProxy.setValueFromProxy();
  BOOST_CHECK_EQUAL(doubleProxy.getType(), StackElement::SE_DOUBLE);
  BOOST_CHECK_CLOSE(doubleProxy, 42.42, 0.0001);
  doubleProxy.clearValueFromProxy();
  BOOST_CHECK_EQUAL(doubleProxy.getType(), StackElement::SE_UNK);
  BOOST_CHECK_EQUAL(doubleProxy.isProxy(), true);
}

BOOST_AUTO_TEST_CASE(testVectorProxy)
{
  StackElement intProxy("vectorIntTEST");
  BOOST_CHECK_EQUAL(intProxy.getType(), StackElement::SE_UNK);
  BOOST_CHECK_EQUAL(intProxy.isProxy(), true);
  BOOST_CHECK_THROW(intProxy += 3, std::runtime_error);
  intProxy.setValueFromProxy();
  BOOST_CHECK_EQUAL(intProxy.getType(), StackElement::SE_VECINT);
  std::vector<int> checkInt(2, 42);
  BOOST_CHECK_EQUAL(intProxy, checkInt);
  intProxy.clearValueFromProxy();
  BOOST_CHECK_EQUAL(intProxy.getType(), StackElement::SE_UNK);
  BOOST_CHECK_EQUAL(intProxy.isProxy(), true);

  StackElement doubleProxy("vectorDoubleTEST");
  BOOST_CHECK_EQUAL(doubleProxy.getType(), StackElement::SE_UNK);
  BOOST_CHECK_EQUAL(doubleProxy.isProxy(), true);
  doubleProxy.setValueFromProxy();
  BOOST_CHECK_EQUAL(doubleProxy.getType(), StackElement::SE_VECDOUBLE);
  std::vector<double> checkDouble(2, 42.42);
  BOOST_CHECK_CLOSE(doubleProxy, checkDouble, 0.0001);
  doubleProxy.clearValueFromProxy();
  BOOST_CHECK_EQUAL(doubleProxy.getType(), StackElement::SE_UNK);
  BOOST_CHECK_EQUAL(doubleProxy.isProxy(), true);
}

BOOST_AUTO_TEST_CASE(testNonExistentProxy)
{
  StackElement doubleProxy("nonExistent");
  BOOST_CHECK_EQUAL(doubleProxy.getType(), StackElement::SE_UNK);
  BOOST_CHECK_THROW(doubleProxy.setValueFromProxy(), std::runtime_error);
}


BOOST_AUTO_TEST_CASE(testCreateVectorElements)
{
  std::vector<int> tempI;
  tempI.push_back(1);
  tempI.push_back(2);
  StackElement vecI(tempI);
  BOOST_CHECK_EQUAL(vecI.getType(), StackElement::SE_VECINT);

  std::vector<double> tempD;
  tempD.push_back(1.0);
  tempD.push_back(2.0);
  StackElement vecD(tempD);
  BOOST_CHECK_EQUAL(vecD.getType(), StackElement::SE_VECDOUBLE);
}

BOOST_AUTO_TEST_CASE(testAssignVectorElements)
{
  std::vector<int> tempI; tempI.push_back(1); tempI.push_back(2);
  StackElement vecI;
  vecI = tempI;
  BOOST_CHECK_EQUAL(vecI.getType(), StackElement::SE_VECINT);
  std::vector<int> tempTrue; tempTrue.push_back(1); tempTrue.push_back(1);
  BOOST_CHECK(vecI == tempI);
  BOOST_CHECK_EQUAL(vecI, tempI);

  std::vector<double> tempD;
  tempD.push_back(1.0);
  tempD.push_back(2.0);
  StackElement vecD;
  vecD = tempD;
  BOOST_CHECK_EQUAL(vecD.getType(), StackElement::SE_VECDOUBLE);
  BOOST_CHECK_CLOSE(vecD, tempD, 0.0001);
}

BOOST_AUTO_TEST_CASE(testIntVectorMath)
{
  std::vector<int> temp1; temp1.push_back(1); temp1.push_back(2);
  StackElement se1(temp1);

  std::vector<int> temp2; temp2.push_back(3); temp2.push_back(4);
  StackElement se2(temp2);

  std::vector<int> temp3; temp3.push_back(4); temp3.push_back(6);
  StackElement se3(temp3);

  std::vector<int> temp4; temp4.push_back(-2); temp4.push_back(-2);
  StackElement se4(temp4);

  se1 -= temp2;
  BOOST_CHECK_EQUAL(se1.getType(), StackElement::SE_VECINT);
  BOOST_CHECK_EQUAL(se1, temp4);
  BOOST_CHECK_EQUAL(se1, se4);
  
  se1 -= 1;
  std::vector<int> temp5; temp5.push_back(-3); temp5.push_back(-3);
  BOOST_CHECK_EQUAL(se1, temp5);

  StackElement derived1;
  derived1 = se1 - 3;
  std::vector<int> temp6; temp6.push_back(-6); temp6.push_back(-6);
  BOOST_CHECK_EQUAL(derived1, temp6);

  StackElement derived2(derived1+3);
  BOOST_CHECK_EQUAL(derived2, temp5);
  derived2 *= 2;
  BOOST_CHECK_EQUAL(derived2, temp6);
  derived2 /= 2;
  BOOST_CHECK_EQUAL(derived2, temp5);

  derived2 = se2 * se3;
  std::vector<int> temp7; temp7.push_back(12); temp7.push_back(24);
  BOOST_CHECK_EQUAL(derived2, temp7);
}

BOOST_AUTO_TEST_CASE(testDoubleVectorMath)
{
  std::vector<double> temp1; temp1.push_back(1.1); temp1.push_back(2.1);
  StackElement se1(temp1);

  std::vector<double> temp2; temp2.push_back(3.1); temp2.push_back(4.1);
  StackElement se2(temp2);

  std::vector<double> temp3; temp3.push_back(4.1); temp3.push_back(6.1);
  StackElement se3(temp3);

  std::vector<double> temp4; temp4.push_back(-2.); temp4.push_back(-2.);
  StackElement se4(temp4);

  se1 -= temp2;
  BOOST_CHECK_EQUAL(se1.getType(), StackElement::SE_VECDOUBLE);
  CHECK_SE_VEC_CLOSE(se1, temp4, 0.0001);
  CHECK_SE_SE_CLOSE(se1, se4, 0.0001);
  
  se1 -= 1;
  std::vector<double> temp5; temp5.push_back(-3.); temp5.push_back(-3.);
  CHECK_SE_VEC_CLOSE(se1, temp5, 0.0001);

  StackElement derived1;
  derived1 = se1 - 3.;
  std::vector<double> temp6; temp6.push_back(-6.); temp6.push_back(-6.);
  CHECK_SE_VEC_CLOSE(derived1, temp6, 0.0001);

  StackElement derived2(derived1+3.);
  CHECK_SE_VEC_CLOSE(derived2, temp5, 0.0001);
  derived2 *= 2.;
  CHECK_SE_VEC_CLOSE(derived2, temp6, 0.0001);
  derived2 /= StackElement(2);
  CHECK_SE_VEC_CLOSE(derived2, temp5, 0.0001);
}

BOOST_AUTO_TEST_CASE(testProblematicVectorMath)
{
  std::vector<int> temp1; temp1.push_back(1); temp1.push_back(2);
  std::vector<int> temp2; temp2.push_back(2); temp2.push_back(3);
  StackElement se1(temp1);
  StackElement se2(1);
  BOOST_CHECK_EQUAL((se2+se1).getType(), StackElement::SE_VECINT); 
  BOOST_CHECK_EQUAL((se2+se1), temp2); 
}

BOOST_AUTO_TEST_CASE(testProblematicDoubleVectorMath)
{
  std::vector<double> temp1; temp1.push_back(1.3); temp1.push_back(2.3);
  std::vector<double> temp2; temp2.push_back(2.3); temp2.push_back(3.3);
  StackElement se1(temp1);
  StackElement se2(1);
  BOOST_CHECK_EQUAL((se2+se1).getType(), StackElement::SE_VECDOUBLE); 
  BOOST_CHECK_EQUAL((se2+se1), temp2); 
}

BOOST_AUTO_TEST_CASE(testTypeInInequalities)
{
  StackElement a(3.4);
  BOOST_CHECK(a > 3);
  BOOST_CHECK(a <= 4);
  BOOST_CHECK(a != 3);
  BOOST_CHECK(a != 4);
  BOOST_CHECK_EQUAL(a == 3, false);
  BOOST_CHECK_EQUAL(a == 4, false);
}

BOOST_AUTO_TEST_CASE(testVectorLogic_tooLong)
{
  std::vector<int> temp1(2, 1);
  StackElement se1(temp1);
  std::vector<int> tooLong(3, 2);
  BOOST_CHECK_THROW(se1._gt(tooLong), std::runtime_error);
  BOOST_CHECK_THROW(se1._gt(StackElement(tooLong)), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(testVectorLogic_gt)
{
  std::vector<int> vTrue(2, 1);
  std::vector<int> vFalse(2, 0);

  std::vector<int> temp1(2, 1);
  StackElement se1(temp1);
  std::vector<int> temp2(2, 2);
  StackElement se2(temp2);

  BOOST_CHECK_EQUAL((se1._gt(se2)).getType(), StackElement::SE_VECINT);
  BOOST_CHECK_EQUAL(se1._gt(se2), vFalse);
  BOOST_CHECK_EQUAL(se2._gt(se1), vTrue);
  BOOST_CHECK_EQUAL(se2._gt(-8), vTrue);
  BOOST_CHECK_EQUAL(se2._gt(8), vFalse);
  BOOST_CHECK_EQUAL(se2._gt(-8.3), vTrue);
  BOOST_CHECK_EQUAL(se2._gt(8.3), vFalse);
  BOOST_CHECK_EQUAL(se2._gt(StackElement(-8)), vTrue);
  BOOST_CHECK_EQUAL(se2._gt(StackElement(8)), vFalse);
  BOOST_CHECK_EQUAL(se2._gt(StackElement(-8.3)), vTrue);
  BOOST_CHECK_EQUAL(se2._gt(StackElement(8.3)), vFalse);
}

BOOST_AUTO_TEST_CASE(testVectorLogic_gte)
{
  std::vector<int> vTT(2, 1);
  std::vector<int> vFF(2, 0);
  std::vector<int> vTF; vTF.push_back(1); vTF.push_back(0);
  std::vector<int> vFT; vFT.push_back(0); vFT.push_back(1);

  std::vector<int> temp1; temp1.push_back(2); temp1.push_back(3);
  StackElement se1(temp1);
  std::vector<int> temp2; temp2.push_back(1); temp2.push_back(3);
  StackElement se2(temp2);

  BOOST_CHECK_EQUAL((se1._gte(se2)).getType(), StackElement::SE_VECINT);
  BOOST_CHECK_EQUAL(se1._gte(se2), vTT);
  BOOST_CHECK_EQUAL(se2._gte(se1), vFT);
  BOOST_CHECK_EQUAL(se2._gte(-8), vTT);
  BOOST_CHECK_EQUAL(se2._gte(8), vFF);
  BOOST_CHECK_EQUAL(se2._gte(-8.3), vTT);
  BOOST_CHECK_EQUAL(se2._gte(8.3), vFF);
  BOOST_CHECK_EQUAL(se2._gte(StackElement(-8)), vTT);
  BOOST_CHECK_EQUAL(se2._gte(StackElement(8)), vFF);
  BOOST_CHECK_EQUAL(se2._gte(StackElement(-8.3)), vTT);
  BOOST_CHECK_EQUAL(se2._gte(StackElement(8.3)), vFF);
}

BOOST_AUTO_TEST_CASE(testVectorLogic_lte)
{
  std::vector<int> vTT(2, 1);
  std::vector<int> vFF(2, 0);
  std::vector<int> vTF; vTF.push_back(1); vTF.push_back(0);
  std::vector<int> vFT; vFT.push_back(0); vFT.push_back(1);

  std::vector<double> temp1; temp1.push_back(2); temp1.push_back(3.4);
  StackElement se1(temp1);
  std::vector<double> temp2; temp2.push_back(1.1); temp2.push_back(3);
  StackElement se2(temp2);

  BOOST_CHECK_EQUAL((se1._lte(se2)).getType(), StackElement::SE_VECINT);
  BOOST_CHECK_EQUAL(se1._lte(se2), vFF);
  BOOST_CHECK_EQUAL(se2._lte(se1), vTT);
  BOOST_CHECK_EQUAL(se2._lte(-8), vFF);
  BOOST_CHECK_EQUAL(se2._lte(8), vTT);
  BOOST_CHECK_EQUAL(se2._lte(-8.3), vFF);
  BOOST_CHECK_EQUAL(se2._lte(8.3), vTT);
  BOOST_CHECK_EQUAL(se2._lte(StackElement(-8)), vFF);
  BOOST_CHECK_EQUAL(se2._lte(StackElement(8)), vTT);
  BOOST_CHECK_EQUAL(se2._lte(StackElement(-8.3)), vFF);
  BOOST_CHECK_EQUAL(se2._lte(StackElement(8.3)), vTT);
}

BOOST_AUTO_TEST_CASE(testVectorLogic_and)
{
  std::vector<int> vTT(2, 1);
  std::vector<int> vFF(2, 0);
  std::vector<int> vTF; vTF.push_back(1); vTF.push_back(0);
  std::vector<int> vFT; vFT.push_back(0); vFT.push_back(1);

  std::vector<int> temp1; temp1.push_back(1); temp1.push_back(0);
  StackElement se1(temp1);
  std::vector<int> temp2; temp2.push_back(1); temp2.push_back(1);
  StackElement se2(temp2);

  BOOST_CHECK_EQUAL((se1._and(se2)).getType(), StackElement::SE_VECINT);
  BOOST_CHECK_EQUAL(se1._and(se2), vTF);
  BOOST_CHECK_EQUAL(se2._and(se1), vTF);
}

BOOST_AUTO_TEST_CASE(testVectorLogic_or)
{
  std::vector<int> vTT(2, 1);
  std::vector<int> vFF(2, 0);
  std::vector<int> vTF; vTF.push_back(1); vTF.push_back(0);
  std::vector<int> vFT; vFT.push_back(0); vFT.push_back(1);

  std::vector<int> temp1; temp1.push_back(1); temp1.push_back(0);
  StackElement se1(temp1);
  std::vector<int> temp2; temp2.push_back(1); temp2.push_back(1);
  StackElement se2(temp2);

  BOOST_CHECK_EQUAL((se1._or(se2)).getType(), StackElement::SE_VECINT);
  BOOST_CHECK_EQUAL(se1._or(se2), vTT);
  BOOST_CHECK_EQUAL(se2._or(se1), vTT);
}

BOOST_AUTO_TEST_CASE(testSumCount)
{
  StackElement se(3);
  BOOST_CHECK_EQUAL(se._sum().getType(), StackElement::SE_INT);
  BOOST_CHECK_EQUAL(se._sum(), 3);
  BOOST_CHECK_EQUAL(se._count().getType(), StackElement::SE_INT);
  BOOST_CHECK_EQUAL(se._count(), 1);

  StackElement se2(3.4);
  BOOST_CHECK_EQUAL(se2._sum().getType(), StackElement::SE_DOUBLE);
  BOOST_CHECK_CLOSE(se2._sum(), 3.4, 0.0001);
  BOOST_CHECK_EQUAL(se2._count().getType(), StackElement::SE_INT);
  BOOST_CHECK_EQUAL(se2._count(), 1);

  StackElement se3(std::vector<int>(2,5));
  BOOST_CHECK_EQUAL(se3._sum().getType(), StackElement::SE_INT);
  BOOST_CHECK_EQUAL(se3._sum(), 10);
  BOOST_CHECK_EQUAL(se3._count().getType(), StackElement::SE_INT);
  BOOST_CHECK_EQUAL(se3._count(), 2);

  StackElement se4(std::vector<double>(2,5.5));
  BOOST_CHECK_EQUAL(se4._sum().getType(), StackElement::SE_DOUBLE);
  BOOST_CHECK_CLOSE(se4._sum(), 11., 0.0001);
  BOOST_CHECK_EQUAL(se4._count().getType(), StackElement::SE_INT);
  BOOST_CHECK_EQUAL(se4._count(), 2);


  StackElement se5(0);
  BOOST_CHECK_EQUAL(se5._count(), 0);
  std::vector<int> temp; temp.push_back(1); temp.push_back(1); temp.push_back(1); temp.push_back(0); temp.push_back(1); temp.push_back(0);
  StackElement se6(temp);
  BOOST_CHECK_EQUAL(se6._count(), 4);
}

BOOST_AUTO_TEST_CASE(testVectorIntSum)
{
  std::vector<int> testVecI; testVecI.push_back(1); testVecI.push_back(2); testVecI.push_back(42);
  StackElement aVecI(testVecI);
  BOOST_CHECK_EQUAL(aVecI._sum(), 45);
}

BOOST_AUTO_TEST_CASE(testVectorDoubleSum)
{
  std::vector<double> testVecD; testVecD.push_back(1.03); testVecD.push_back(2.343); testVecD.push_back(42.5939);
  StackElement aVecD(testVecD);
  BOOST_CHECK_CLOSE(aVecD._sum(), 45.9669, 0.0001);
}

BOOST_AUTO_TEST_CASE(testAbs)
{
  std::vector<double> testVecD; testVecD.push_back(1.03); testVecD.push_back(2.343); testVecD.push_back(-42.5939);
  std::vector<double> testVecD2; testVecD2.push_back(1.03); testVecD2.push_back(2.343); testVecD2.push_back(42.5939);
  StackElement aVecD(testVecD);
  CHECK_SE_VEC_CLOSE(aVecD._abs(), testVecD2, 0.0001);

  StackElement se(-2);
  BOOST_CHECK_EQUAL(se._abs().getType(), StackElement::SE_INT);
  BOOST_CHECK_EQUAL(se._abs(), 2);

  StackElement se2(-2.3);
  BOOST_CHECK_EQUAL(se2._abs().getType(), StackElement::SE_DOUBLE);
  BOOST_CHECK_CLOSE(se2._abs(), 2.3, 0.0001);
}

BOOST_AUTO_TEST_CASE(testSqrt)
{
  std::vector<double> testVecD; testVecD.push_back(1.); testVecD.push_back(9.); testVecD.push_back(3.);
  std::vector<double> testVecD2; testVecD2.push_back(1.); testVecD2.push_back(3.); testVecD2.push_back(1.732050808);
  StackElement aVecD(testVecD);
  CHECK_SE_VEC_CLOSE(aVecD._sqrt(), testVecD2, 0.0001);

  StackElement se(9);
  BOOST_CHECK_EQUAL(se._sqrt().getType(), StackElement::SE_DOUBLE);
  BOOST_CHECK_CLOSE(se._sqrt(), 3., 0.0001);

  StackElement se2(9.);
  BOOST_CHECK_EQUAL(se2._sqrt().getType(), StackElement::SE_DOUBLE);
  BOOST_CHECK_CLOSE(se2._sqrt(), 3., 0.0001);
}

BOOST_AUTO_TEST_CASE(testVectorNegation)
{
  std::vector<int> testVecI; testVecI.push_back(1); testVecI.push_back(2); testVecI.push_back(42);
  StackElement aVecI(testVecI);
  std::vector<int> testVecI2; testVecI2.push_back(-1); testVecI2.push_back(-2); testVecI2.push_back(-42);
  StackElement aVecI2(testVecI2);
  BOOST_CHECK_EQUAL(-aVecI, aVecI2);
}

BOOST_AUTO_TEST_CASE(testVectorUnaryNot)
{
  std::vector<int> testVecI; testVecI.push_back(0); testVecI.push_back(2); testVecI.push_back(1);
  StackElement aVecI(testVecI);
  std::vector<int> testVecI2; testVecI2.push_back(1); testVecI2.push_back(0); testVecI2.push_back(0);
  StackElement aVecI2(testVecI2);
  BOOST_CHECK_EQUAL(!aVecI, aVecI2);
}

BOOST_AUTO_TEST_CASE(testPowers)
{
  StackElement el(2);
  StackElement el2 = el._pow(2);
  BOOST_CHECK_EQUAL(el2.getType(), StackElement::SE_DOUBLE);
  BOOST_CHECK_CLOSE(el2, 4, 0.0001);

  std::vector<int> testVecI; testVecI.push_back(2); testVecI.push_back(3);
  std::vector<double> testVecD; testVecD.push_back(4.); testVecD.push_back(9.);
  StackElement el3(testVecI);
  StackElement el4(testVecD);
  BOOST_CHECK_EQUAL((el3._pow(2)).getType(), StackElement::SE_VECDOUBLE);
  CHECK_SE_SE_CLOSE(el3._pow(2), el4, 0.0001);

  BOOST_CHECK_THROW(el3._pow(el4), std::runtime_error);
}

// TODO
//BOOST_AUTO_TEST_CASE(testIntDivide)
//{
//  StackElement a(1);
//  StackElement b(2);
//  BOOST_CHECK_EQUAL((a/b).getType(), StackElement::SE_DOUBLE);
//  BOOST_CHECK_CLOSE(a/b, 0.5, 0.0001);
//}

BOOST_AUTO_TEST_SUITE_END()
