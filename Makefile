CFLAGS=-I/opt/local/include -Wall -pedantic
all: StackElement_test ExpressionParser_test test

clean:
	rm StackElement_test
	rm ExpressionParser_test

calc6: calc6.cxx StackElement.h StackElement.icc
	clang++ $(CFLAGS) calc6.cxx TestProxyLoader.cxx StackElement.cxx ParsingInternals.cxx IUnitInterpreter.cxx NTUPUnitInterpreter.cxx -o calc6
	./calc6

StackElement_test: StackElement_test.cxx StackElement.h StackElement.cxx StackElement.icc IProxyLoader.h TestProxyLoader.h TestProxyLoader.cxx ParsingInternals.h ParsingInternals.cxx
	clang++ $(CFLAGS) StackElement_test.cxx StackElement.cxx TestProxyLoader.cxx ParsingInternals.cxx IUnitInterpreter.cxx NTUPUnitInterpreter.cxx -o StackElement_test

ExpressionParser_test: ExpressionParser_test.cxx ExpressionParser.cxx StackElement.h StackElement.cxx StackElement.icc IProxyLoader.h TestProxyLoader.h TestProxyLoader.cxx ParsingInternals.h ParsingInternals.cxx
	clang++ $(CFLAGS) ExpressionParser_test.cxx TestProxyLoader.cxx ExpressionParser.cxx StackElement.cxx ParsingInternals.cxx IUnitInterpreter.cxx NTUPUnitInterpreter.cxx -o ExpressionParser_test

test: StackElement_test ExpressionParser_test
	./StackElement_test
	./ExpressionParser_test
