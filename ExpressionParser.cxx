/////////////////////////////////////////////////////////////////
// ExpressionParser.cxx, (c) ATLAS Detector software
/////////////////////////////////////////////////////////////////
// Author: Thomas Gillam (thomas.gillam@cern.ch)
// ExpressionParsing library
/////////////////////////////////////////////////////////////////

#include "ExpressionParser.h"
#include "ParsingInternals.h"
#include "ProxyLoaderSingleton.h"
#include "UnitInterpreterSingleton.h"

namespace ExpressionParsing {

  ExpressionParser::ExpressionParser()
  {
    setup();
  }

  ExpressionParser::ExpressionParser(IProxyLoader *proxyLoader)
  {
    ProxyLoaderSingleton::setInstance(proxyLoader);
    setup();
  }

  ExpressionParser::ExpressionParser(IUnitInterpreter *unitInterpreter)
  {
    UnitInterpreterSingleton::setInstance(unitInterpreter);
    setup();
  }

  ExpressionParser::ExpressionParser(IProxyLoader *proxyLoader, IUnitInterpreter *unitInterpreter)
  {
    ProxyLoaderSingleton::setInstance(proxyLoader);
    UnitInterpreterSingleton::setInstance(unitInterpreter);
    setup();
  }

  ExpressionParser::~ExpressionParser()
  {
    if (vm) {
      delete vm;
    }
  }

  void ExpressionParser::setup()
  {
    vm = new VirtualMachine();
  }

  bool ExpressionParser::loadExpression(const std::string &expression)
  {
    ProxyLoaderSingleton::getInstance()->reset();
    code.clear();

    Grammar<std::string::const_iterator> grammar;
    ast::expression expr;

    std::string::const_iterator begin = expression.begin();
    std::string::const_iterator end = expression.end();
    using boost::spirit::ascii::space_type;
    bool r = phrase_parse(begin, end, grammar, space_type(), expr);
    if (!r) return false;

    Compiler compiler(code);
    compiler(expr);
    return true;
  }

  StackElement ExpressionParser::evaluate()
  {
    vm->execute(code);
    return vm->top();
  }

  bool ExpressionParser::evaluateAsBool()
  {
    const StackElement &result = evaluate();
    if (result.isScalar()) return result.asBool();
    else throw std::runtime_error("Unable to evaluate vector quantity as a boolean");
  }
}
